// DB
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
const config = require("../constants/config");
module.exports = function (fileName, text) {
  MongoClient.connect(url, function (err, db) {
    if (err) throw err;
    var dbo = db.db(config.DB);
    var datetime = new Date();
    var myobj = { namefile: fileName, text: text, datetime };
    dbo.collection(config.COLLECTION).insertOne(myobj, function (err, res) {
      if (err) throw err;
      console.log("1 document inserted");
      db.close();
    });
  });
}
