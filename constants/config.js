const detail = require("./config.json");
//Name database Mongodb
const DB = detail.dbname;
// Name collection                          
const COLLECTION = detail.collection;
// Name google cloud storage bucket  
const BUCKET_NAME = detail.bucketname;
// Name folder audio in GCS  
const FOLDER_NAME = detail.foldername;        
// Audio
const ENCODING = detail.encoding;
const SAMPLERATEHERTZ  = detail.sampleRateHertz;
module.exports = {
    DB,
    COLLECTION,
    BUCKET_NAME,
    FOLDER_NAME,
    ENCODING,
    SAMPLERATEHERTZ
};