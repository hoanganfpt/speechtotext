module.exports = function (bucketName, folderName) {
    // [START speech_transcribe_sync_gcs]
    // Imports the Google Cloud client library
    const speech = require('@google-cloud/speech');
    // DB
    const db = require('../database/index.js');
    // Creates a client
    const client = new speech.SpeechClient();
    // Config format audio
    const config = require("../constants/config");
    const encoding = config.ENCODING;
    const sampleRateHertz = config.SAMPLERATEHERTZ;
    const languageCode = 'vi-VN';
    function syncRecognizeGCS(gcsUri, fileName) {
        const config = {
            encoding: encoding,
            sampleRateHertz: sampleRateHertz,
            languageCode: languageCode,
        };
        const audio = {
            uri: gcsUri,
        };

        const request = {
            config: config,
            audio: audio,
        };

        // Detects speech in the audio file
        client
            .recognize(request)
            .then(data => {
                const response = data[0];
                const transcription = response.results
                    .map(result => result.alternatives[0].transcript)
                    .join('\n');
                db(fileName, transcription);
            })
            .catch(err => {
                asyncRecognizeGCS(gcsUri, fileName);
            });
        // [END speech_transcribe_sync_gcs]
    }

    function asyncRecognizeGCS(gcsUri, fileName) {
        const config = {
            encoding: encoding,
            sampleRateHertz: sampleRateHertz,
            languageCode: languageCode,
        };

        const audio = {
            uri: gcsUri,
        };

        const request = {
            config: config,
            audio: audio,
        };

        // Detects speech in the audio file. This creates a recognition job that you
        // can wait for now, or get its result later.
        client
            .longRunningRecognize(request)
            .then(data => {
                const operation = data[0];
                // Get a Promise representation of the final result of the job
                return operation.promise();
            })
            .then(data => {
                const response = data[0];
                const transcription = response.results
                    .map(result => result.alternatives[0].transcript)
                    .join('\n');
                db(fileName, transcription);
            })
            .catch(err => {
                console.error('ERROR:', err);
            });
        // [END speech_transcribe_async_gcs]
    }
    /// Run foreach file

    async function listFilesByPrefix(bucketName, prefix, delimiter) {
        // [START storage_list_files_with_prefix]
        // Imports the Google Cloud client library
        const { Storage } = require('@google-cloud/storage');

        // Creates a client
        const storage = new Storage();
        const options = {
            prefix: prefix,
        };

        if (delimiter) {
            options.delimiter = delimiter;
        }

        // Lists files in the bucket, filtered by a prefix
        const [files] = await storage.bucket(bucketName).getFiles(options);
        files.forEach(file => {
            syncRecognizeGCS("gs://" + bucketName + "/" + file.name, file.name);
        });
        // [END storage_list_files_with_prefix]
    }

    listFilesByPrefix(bucketName, folderName + "/", "/");
}