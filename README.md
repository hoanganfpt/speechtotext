# SpeechToText

Speech to text using Google Cloud Speech API
1. Install MongoDB , MongoDB compass
    - Create Database, Collection
    - Download MongoDB : https://www.mongodb.com/download-center/v2/community
    - Download MongoDB compass : https://www.mongodb.com/download-center/v2/compass
2. Install NodeJs
    - Download at : https://nodejs.org/en/
3. Set up a Google Cloud Platform project.
    - Create or select a project.
    - Enable the Google Speech-to-Text API for that project.
    - Create a service account.
    - Download a private key as JSON.
    - Reference: https://cloud.google.com/speech-to-text/docs/quickstart-client-libraries
4. Set the environment variable GOOGLE_APPLICATION_CREDENTIALS to the file path of the JSON file that contains your service account key. This variable only applies to your current shell session, so if you open a new session, set the variable again.
    Linux or macOS: export GOOGLE_APPLICATION_CREDENTIALS="[PATH]"
    Windows : 
        - With PowerShell: $env:GOOGLE_APPLICATION_CREDENTIALS="[PATH]"
        - With command prompt: set GOOGLE_APPLICATION_CREDENTIALS=[PATH]
5. Install and initialize the Cloud SDK:  https://cloud.google.com/sdk/docs/
6. Google cloud storage
    - Select or create a GCP project.
    - Make sure that billing is enabled for your project.
    - Create a bucket
    - Upload folder file audio to Google Cloud Storage in cmd:  
            gsutil -m cp -r [Folder] gs://[Bucketname]
    - Reference: https://cloud.google.com/storage/docs/quickstarts
7. Run project:
    - Clone project
    - Run cmd : Set key cho cmd : set GOOGLE_APPLICATION_CREDENTIALS=[PATH]
    - Edit config file : ./constants/config.json
        - "dbname" :     [DatabaseName]
        - "collection" : [CollectionName],
        - "bucketname" : [BucketName],
        - "foldername" : [FolderNameInBucketName],
    - Run cmd : npm start
    - Transcription saved in Database 
